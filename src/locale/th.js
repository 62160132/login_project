export default {
  weekDays: ['จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์', 'อาทิตย์'],
  weekDaysShort: ['จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส', 'อ'],
  years: 'ปี',
  year: 'ปีนี้',
  month: 'เดือน',
  week: 'สัปดาห์',
  day: 'วัน',
  today: 'วันนี้',
  noEvent: 'ไม่มีกิจกรรม',
  allDay: 'ทั้งวัน',
  daleteEvent: 'ลบ',
  createEvent: 'กิจกรรมใหม่',
  dateFormat: 'YYYY MMMM D dddd'
}
